class BaseParam(object):
    def __init__(self, request):
        self.proto = request.headers.get('X-Forwarded-Proto', None)
        self.real_ip = request.headers.get('X-Real-IP', None)
        self.forwarded_for = request.headers.get('X-Forwarded-For', None)
        self.remote = str(request.remote)
        self.method = str(request.method)
        self.relative_url = str(request.url.relative())

    @property
    def ip(self):
        if self.real_ip:
            return self.real_ip
        else:
            # TODO: Use logging
            # print('X-Real-IP not found. Doing a test?')
            return self.remote


class SearchParam(BaseParam):
    def __init__(self, request):
        super().__init__(request)

        self.ids = request.query.getall('id', [])
        self.padding_size = int(request.query.get('padding', 0))
        self.padding_location = int(request.query.get('location', 0))
        self.mixed = int(request.query.get('mixed', 1))
