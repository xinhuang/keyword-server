from sqlalchemy import (
    MetaData, create_engine,
    Table, Column, ForeignKey,
    Integer, String, Date,
)
from databases import Database
import datetime
from typing import List


__all__ = ['keyword', 'log', 'KeywordDB', 'ensure_table']


keyword = Table(
    'keyword', MetaData(),

    Column('id', Integer, primary_key=True),
    Column('text', String(200), nullable=False),
    Column('last_queried', String(30), nullable=True),
    Column('created', String(30), nullable=False),
    Column('deprecated', Integer, nullable=False),
)

log = Table(
    'log', MetaData(),

    Column('timestamp', String(30), nullable=False),
    Column('ip', String(60), nullable=False),
    Column('method', String(20), nullable=False),
    Column('parameter', String(200), nullable=False),
)


def ensure_table(conf):
    kengine = create_engine(conf['keyword']['connection-string'])
    keyword.metadata.create_all(bind=kengine, tables=[keyword])

    lengine = create_engine(conf['log']['connection-string'])
    log.metadata.create_all(bind=lengine, tables=[log])


async def init(app):
    conf = app['config']['sqlite']

    ensure_table(conf)

    kdb = KeywordDB(Database(conf['keyword']['connection-string']))
    await kdb.connect()
    app['keyword-db'] = kdb

    ldb = LogDB(Database(conf['log']['connection-string']))
    await ldb.connect()
    app['log-db'] = ldb


async def close(app) -> None:
    await app['keyword-db'].disconnect()
    await app['log-db'].disconnect()


class LogDB(object):
    def __init__(self, db: Database):
        self.db = db

    async def connect(self):
        await self.db.connect()

    async def disconnect(self):
        await self.db.disconnect()

    async def log(self, ip: str, method: str, parameter: str):
        ts = datetime.datetime.utcnow()
        query = log.insert()
        p = parameter[:200]
        values = {'ip': ip, 'method': method,
                  'timestamp': ts.isoformat(), 'parameter': p, }
        await self.db.execute(query=query, values=values)


class KeywordDB(object):
    def __init__(self, db: Database):
        self.db = db

    async def connect(self):
        await self.db.connect()

    async def disconnect(self):
        await self.db.disconnect()

    async def get(self, id):
        query = keyword.select().where(keyword.c.id == id)
        records = await self.db.fetch_all(query=query)
        return dict(records[0]) if records else None

    async def get_all(self, column: str = None):
        query = keyword.select()
        records = await self.db.fetch_all(query=query)
        if column is None:
            return [dict(q) for q in records]
        else:
            return [q[column] for q in records]

    async def touch(self, ids: List[int], ts=datetime.datetime.utcnow()) -> None:
        for the_id in ids:
            ts = datetime.datetime.utcnow()
            query = keyword.update().where(keyword.c.id == the_id).values(last_queried=ts.isoformat())
            await self.db.execute(query)
