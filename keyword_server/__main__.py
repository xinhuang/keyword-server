#!/usr/bin/env python
# $ printf 'echo\r\n' | nc localhost 8000
# echo
import asyncio
import logging
import os
import aiosqlite
from aiohttp import web
import aiohttp_jinja2
import jinja2
from datetime import datetime

from keyword_server import db
from keyword_server import route


if __name__ == '__main__':
    logging.basicConfig()
    logger = logging.getLogger('kwserver')
    logger.setLevel(logging.DEBUG)

    host = os.environ.get('MY_HOST', '0.0.0.0')
    port = os.environ.get('PORT', 9000)
    verbose = os.environ.get('verbose')
    print(f'Set environment variable MY_HOST and PORT for listening address')
    app = web.Application()
    app['config'] = {
        'sqlite': {
            'keyword': {
                'connection-string': 'sqlite:///keyword-server.db',
            },
            'log': {
                'connection-string': 'sqlite:///log.db',
            }
        },
        'host': host,
        'port': port,
        'verbose': verbose is not None,
    }

    aiohttp_jinja2.setup(
        app, loader=jinja2.PackageLoader('keyword_server', 'templates'))

    app.on_startup.append(db.init)
    app.on_cleanup.append(db.close)

    route.setup(app)

    web.run_app(app, host=host, port=port)
