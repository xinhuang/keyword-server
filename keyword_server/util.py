from databases import Database
import datetime
import pathlib


from . import db
from .lorem import Lorem


PROJECT_ROOT = pathlib.Path(__file__).parent
lorem = Lorem(PROJECT_ROOT / 'data/word-zh.txt')


def random_text(size: int) -> str:
    r = ''
    while len(r) < size:
        r += lorem.paragraph() + '\n'
    return r


async def import_from_file(database: Database, path: str) -> None:
    with open(path) as f:
        lines = [l.strip() for l in f.readlines()]
    lines = [l for l in lines if l]

    query = db.keyword.select().order_by(db.keyword.c.id)
    max_id = int(await database.fetch_val(query=query, column='id') or 0)

    query = db.keyword.insert()
    values = []
    now = datetime.datetime.utcnow().isoformat()
    for l in lines:
        values.append({'id': max_id + 1, 'text': l, 'last_queried': None,
                       'created': now, 'deprecated': 0, })
        max_id += 1
    await database.execute_many(query=query, values=values)
