import random


PAUSES = ['，', '，', '，', '，', '、', '；', '：', ]
STOPS = ['。', '。', '。', '。', '。', '。', '。', '。', '。', '。', '！', '？', ]
CHARS_PER_SENTENCE = (5, 15)
SENTENCE_PER_PAR = (3, 10)
CHARS_PER_PARAGRAPH = (CHARS_PER_SENTENCE[0] * SENTENCE_PER_PAR[0],
                       CHARS_PER_SENTENCE[1] * SENTENCE_PER_PAR[1])


class Lorem(object):
    def __init__(self, words_path: str):
        self.path = words_path
        self._load_words()

    def _load_words(self):
        with open(self.path) as f:
            lines = [l.strip() for l in f.readlines()]
            self.words = list([l for l in lines if l])

    def word(self):
        return random.choice(self.words)

    def subsentence(self, length: int) -> str:
        s = ''
        while len(s) < length:
            s += self.word()
        return s[:length]

    def sentence(self, length: int = None) -> str:
        length = length or random.choice(range(CHARS_PER_SENTENCE[0], CHARS_PER_SENTENCE[1]))

        s = ''
        while len(s) < length - 1:
            l = min(length - len(s) - 1,
                    random.choice(range(CHARS_PER_SENTENCE[0], CHARS_PER_SENTENCE[1])))
            part = self.subsentence(l)
            s += part + random.choice(PAUSES)
        s = s[:-1] + random.choice(STOPS)
        return s

    def paragraph(self, length: int = None) -> str:
        length = length or random.choice(range(CHARS_PER_PARAGRAPH[0], CHARS_PER_PARAGRAPH[1]))
        p = ''
        while len(p) < length:
            l = min(length - len(p),
                    random.choice(range(CHARS_PER_SENTENCE[0], CHARS_PER_SENTENCE[1])))
            if length - len(p) - l < CHARS_PER_SENTENCE[0]:
                l = length - len(p)
            p += self.sentence(l)
        return p
