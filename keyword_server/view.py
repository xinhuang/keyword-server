import datetime
from itertools import islice
import asyncio
from random import shuffle
import aiohttp_jinja2
from aiohttp import web

from . import util
lorem = util.lorem
from . import db
from . import req


@aiohttp_jinja2.template('index.html')
async def index(request):
    # query = db.keyword.insert()
    # values = [
    #     {'id': 2, "text": "example2", "last_queried": datetime.datetime.utcnow()},
    #     {'id': 3, "text": "example3", "last_queried": datetime.datetime.utcnow()},
    # ]

    keywords = await request.app['keyword-db'].get_all()
    return {'keywords': []}


@aiohttp_jinja2.template('falundafa.html')
async def falundafa(request):
    pass


@aiohttp_jinja2.template('whatisthis.html')
async def whatisthis(request):
    '''
    Echo keyword with optional padding

    Parameters
    ----------
    k : str
        Word to echo
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together.

    Example
    -------
    GET /search?k=ABC
    '''
    param = req.SearchParam(request)

    if request.app['config']['verbose']:
        print(f'{param.ip}, {param.method}, {param.relative_url}')

    asyncio.get_event_loop().create_task(
        request.app['log-db'].log(param.ip, param.method,
                                  param.relative_url))

    k = request.query.get('k', 'Huh?')
    padding_size = param.padding_size
    padding_location = param.padding_location
    mixed = param.mixed

    keywords = [{'id': -1, 'text': k, 'surrounding': lorem.sentence(), }]

    if padding_location & 1:
        before = lorem.paragraph(padding_size)
    else:
        before = None
    if padding_location & 2:
        after = lorem.paragraph(padding_size)
    else:
        after = None

    return {'keywords': keywords,  'before': before, 'after': after, 'mixed': mixed}


@aiohttp_jinja2.template('whatisthis2.html')
async def whatisthis2(request):
    k = request.query.get('k', 'Huh?')
    return {'k': k}


@aiohttp_jinja2.template('good.html')
async def good(request):
    pass


@aiohttp_jinja2.template('bad.html')
async def bad(request):
    pass


@aiohttp_jinja2.template('sitemap.html')
async def sitemap(request):
    '''
    Parameters
    ----------
    k : Optional[int]
        The number of keywords for a single URL. Default is 1.
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    n : int
        Maximum number of items returned. Default it's 1000. Maximum is 10000.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together (default).

    Example
    -------
    GET /sitemap?k=3&padding=1000&location=3&size=100
    '''
    k = int(request.query.get('k', 1))
    padding_size = int(request.query.get('padding', 0))
    padding_location = int(request.query.get('location', 0))
    n = min(10000, int(request.query.get('n', 1000)))
    mixed = int(request.query.get('mixed', 1))
    host = request.app['config']['host']
    port = request.app['config']['port']

    ids = list(await request.app['keyword-db'].get_all('id'))
    shuffle(ids)

    qstrs = []
    for i in range(k, len(ids), k):
        qs = [f"id={ids[i - j]}" for j in range(1, k + 1)]
        qstrs.append('&'.join(qs))

    if len(ids) % k > 0:
        r = len(ids) % k
        qstrs.append('&'.join([f"id={ids[j]}"
                               for j in range(len(ids) - r, len(ids))]))

    urls = list(islice([f'/search?{qs}&padding={padding_size}&location={padding_location}&mixed={mixed}'
                        for qs in qstrs], n))
    return {'urls': urls}


@aiohttp_jinja2.template('search.html')
async def search(request):
    '''
    Query keywords by ID(s) via a GET request.

    Parameters
    ----------
    id : [int]
        IDs of keywords.
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together.

    Example
    -------
    GET /search?id=1&id=2
    '''
    param = req.SearchParam(request)

    asyncio.get_event_loop().create_task(
        request.app['log-db'].log(param.ip, param.method,
                                  param.relative_url))

    if request.app['config']['verbose']:
        print(f'{param.ip}, {param.method}, {param.relative_url}')

    ids = param.ids
    padding_size = param.padding_size
    padding_location = param.padding_location
    mixed = param.mixed

    keywords = []

    for id in ids:
        k = await request.app['keyword-db'].get(id)
        if k:
            k['surrounding'] = lorem.sentence()
            keywords.append(k)

    if padding_location & 1:
        before = lorem.paragraph(padding_size)
    else:
        before = None
    if padding_location & 2:
        after = lorem.paragraph(padding_size)
    else:
        after = None

    asyncio.get_event_loop().create_task(request.app['keyword-db'].touch(ids))

    return {'keywords': keywords,  'before': before, 'after': after, 'mixed': mixed}
