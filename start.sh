git clone https://gitlab.com/xinhuang/keyword-server.git
cd keyword-server
virtualenv -p python3 .

source bin/activate
pip install -r requirements.txt
deactivate

screen -S ks_ipv4 -dm bash -c "source bin/activate; export MY_HOST='0.0.0.0'; export PORT=8000; python3 -m keyword_server"
screen -S ks_ipv6 -dm bash -c "source bin/activate; export MY_HOST='::'; export PORT=9000; python3 -m keyword_server"

screen -list
